<?php
$colors = array(
    0 => "Белый",
    1 => "Желтый",
    2 => "Оранжевый",
    3 => "Красный",
    4 => "Розовый",
    5 => "Сиреневый",
    6 => "Голубой",
    7 => "Синий",
    8 => "Зеленый",
    9 => "Серый",
    10 => "Коричневый",
    11 => "Бежевый",
    12 => "Серебро",
    13 => "Черный"
);

$fillings = array(
    0 => "Стекло 6мм",
    1 => "Стеклопакет 24мм",
    2 => "Сэндвич 24мм",
    3 => "Сэндвич 24мм + зеркало",
    4 => "Зеркало + Зеркало",
    5 => "Сэндвич 24мм + аракал(1 стор)",
    6 => "Сэндвич 24мм + аракал(2 стор)",
    7 => "Сэндвич 24мм + оцинковка(1 стор)",
    8 => "Сэндвич 24мм + оцинковка(2 стор)",
    9 => "Сэндвич 24мм + фотопечать"
);

function ValidateEmail($email)
{
    $pattern = '/^([0-9a-z]([-.\w]*[0-9a-z])*@(([0-9a-z])+([-\w]*[0-9a-z])*\.)+[a-z]{2,6})$/i';
    return preg_match($pattern, $email);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //$mailto = 'door@tdaluma.ru'; debug:
    $mailto   = 'sitennov@yandex.ru';
    $mailfrom = isset($_POST['email']) ? $_POST['email'] : $mailto;
    $mailcc   = 'dedov@sitennov.ru';
    $subject  = 'Заказ по расчету двери с сайта';
    $eol      = "\r\n";
    
    //Параметры заказа
    $height1    = $_POST['height'];
    $height2    = $_POST['height2'];
    $length     = $_POST['door_length'];
    $impost     = $_POST['impost'];
    $two_sided  = $_POST['one_or_two'];
    $color1     = $_POST['coloring1'];
    $color1_2   = $_POST['coloring1_2'];
    $color2     = $_POST['coloring2'];
    $knob       = $_POST['handle'];
    $filling1   = $_POST['filling'];
    $filling1_2 = $_POST['filling1'];
    $filling2   = $_POST['filling2'];
    $closer     = $_POST['closer'];
    $price      = $_POST['price'];
    $name       = $_POST['name'];
    $phone      = $_POST['phone'];
    $address    = $_POST['adres']; //что это и зачем?
    //
    
    $message = 'Поступил новый заказ двери с сайта' . $eol . '
    Высота: ' . $height1 . $eol;
    if ($impost) {
        $message .= 'С имостом, высота до имоста: .' . $height2 . $eol;
    }
    $message .= 'Ширина: ' . $length . $eol;
    if ($two_sided) {
        $message .= 'С двусторонним окрашиванием, цвет внешней стороны: ' . $colors[$color1_2] . ', цвет обратной стороны: ' . $colors[$color2] . $eol;
    } else {
        $message .= 'С односторонним окрашиванием, цвет: ' . $colors[$color1] . $eol;
    }
    $message .= 'Ручка: ' . $knob . $eol;
    if ($impost) {
        $message .= 'Верхнее заполнение: ' . $fillings[$filling1_2] . ', нижнее: ' . $fillings[$filling2] . $eol;
    } else {
        $message .= 'Заполнение: ' . $fillings[$filling1] . $eol;
    }
    if ($closer) {
        $message .= 'С доводчиком' . $eol;
    } else {
        $message .= 'Без доводчика' . $eol;
    }
    $message .= 'Суммарная цена: ' . $price . $eol . 'Имя заказчика: ' . $name . $eol . 'Телефон: ' . $phone . $eol . 'Адрес: ' . $address . $eol;
    
    $success_url  = './good.html';
    $error_url    = '';
    $error        = '';
    $max_filesize = isset($_POST['filesize']) ? $_POST['filesize'] * 1024 : 1024000;
    $boundary     = md5(uniqid(time()));
    $header       = 'From: ' . $mailfrom . $eol;
    $header .= 'Reply-To: ' . $mailfrom . $eol;
    $header .= 'Cc: ' . $mailcc . $eol;
    $header .= 'MIME-Version: 1.0' . $eol;
    $header .= 'Content-Type: multipart/mixed; boundary="' . $boundary . '"' . $eol;
    $header .= 'X-Mailer: PHP v' . phpversion() . $eol;
    if (!ValidateEmail($mailfrom)) {
        $error .= "The specified email address is invalid!\n<br>";
    }
    
    if (!empty($error)) {
        $errorcode = file_get_contents($error_url);
        $replace   = "##error##";
        $errorcode = str_replace($replace, $error, $errorcode);
        echo $errorcode;
        exit;
    }
    
    $internalfields = array(
        "submit",
        "reset",
        "send",
        "captcha_code"
    );
    $message .= $eol;
    /*foreach ($_POST as $key => $value) {
    if (!in_array(strtolower($key), $internalfields)) {
    if (!is_array($value)) {
    $message .= ucwords(str_replace("_", " ", $key)) . " : " . $value . $eol;
    } else {
    $message .= ucwords(str_replace("_", " ", $key)) . " : " . implode(",", $value) . $eol;
    }
    }
    }*/
    
    $body = 'This is a multi-part message in MIME format.' . $eol . $eol;
    $body .= '--' . $boundary . $eol;
    $body .= 'Content-Type: text/plain; charset=UTF-8' . $eol;
    $body .= 'Content-Transfer-Encoding: 8bit' . $eol;
    $body .= $eol . stripslashes($message) . $eol;
    if (!empty($_FILES)) {
        foreach ($_FILES as $key => $value) {
            if ($_FILES[$key]['error'] == 0 && $_FILES[$key]['size'] <= $max_filesize) {
                $body .= '--' . $boundary . $eol;
                $body .= 'Content-Type: ' . $_FILES[$key]['type'] . '; name=' . $_FILES[$key]['name'] . $eol;
                $body .= 'Content-Transfer-Encoding: base64' . $eol;
                $body .= 'Content-Disposition: attachment; filename=' . $_FILES[$key]['name'] . $eol;
                $body .= $eol . chunk_split(base64_encode(file_get_contents($_FILES[$key]['tmp_name']))) . $eol;
            }
        }
    }
    $body .= '--' . $boundary . '--' . $eol;
    if ($mailto != '') {
        mail($mailto, $subject, $body, $header);
    }
    header('Location: ' . $success_url);
    exit;
}
?>