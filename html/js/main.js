(function($){ 
	"use strict";

	$('.calc__tooltip').each(function() {
		var thisContent = $(this).attr('data-tooltip');
		var posL = $(this).position();
		$(this).hover(function() {
			$('<div class="calc__tip">' + thisContent + '</div>').insertBefore(this);
			$('.calc__tip').css('left', posL.left)
		}, function() {
			$('.calc__tip').remove();
		})
	})

	$('.about__grid-hidden').hide();
	$('.about__show-all').click(function() {
		$('.about__grid-hidden').slideToggle();
		$('html, body').animate({
			scrollTop: $('.about__grid-hidden').offset().top
		}, 2000);
		return false;
	})
	
		$('.zakaz').hide();
	$('.showzakaz').click(function() {
		$('.zakaz').slideToggle();
		$('html, body').animate({
			scrollTop: $('.showzakaz').offset().top
		}, 2000);
		return false;
	})

})(jQuery);