var colors = {
    0: "white.png",
    1: "yellow.png",
    2: "orange.png",
    3: "red.png",
    4: "pink.png",
    5: "purple.png",
    6: "azure.png",
    7: "blue.png",
    8: "green.png",
    9: "gray.png",
    10: "brown.png",
    11: "beige.png",
    12: "silver.png",
    13: "black.png"
};
var fillings = {
    0: "glass.png",
    1: "pack.png",
    2: "sandwich.png",
    3: "sandwichmirror.png",
    4: "mirror.png",
    5: "sandwicharacal.png",
    6: "sandwicharacal.png",
    7: "sandwichgalvan.png",
    8: "sandwichgalvan.png",
    9: "sandwichphoto.png"
}
var knobs = {
    0: "knob-1.png",
    1: "knob-default.png",
    2: "knob-2.png"
}

function call_calculate() {
    var form = document.forms["door_form"];
    var impost = form.elements["impost"].checked;
    var amount = 1; //parseInt(form.elements["amount"].value);
    var height = parseFloat(form.elements["height"].value.replace(",", ".")) / 1000.0;
    var height2 = parseFloat(form.elements["height2"].value.replace(",", ".")) / 1000.0;
    var length = parseFloat(form.elements["door_length"].value.replace(",", ".")) / 1000.0;
    var one_side = form.elements["one_or_two"].checked;
    var coloring1;
    if (!one_side) {
        coloring1 = parseInt(form.elements["coloring1"].value);
    } else {
        coloring1 = parseInt(form.elements["coloring1_2"].value);
    }
    var coloring2 = parseInt(form.elements["coloring2"].value);
    var closer = form.elements["closer"].checked;
    var handle = parseInt(form.elements["handle"].value);
    var filling1;
    if (!impost) {
        filling1 = parseInt(form.elements["filling"].value);
    } else {
        filling1 = parseInt(form.elements["filling1"].value);
    }
    var filling2 = parseInt(form.elements["filling2"].value);
    var full_price = calculate(impost, height, height2, length, amount, coloring1, handle, closer, filling1, filling2, one_side);
    document.door_form.elements['price'].value = full_price;
    displayResults(full_price);
}

function set_impost() {
    var impost = document.door_form.elements["impost"].checked;
    if (impost) {
        document.getElementById("filling_select1").style.display = "none";
        document.getElementById("filling_select1_2").style.display = "block";
        document.getElementById("filling_select2").style.display = "block";
        document.getElementById("height2_div").style.display = "block";
        document.getElementById("without").style.display = "none";
        document.getElementById("top").style.display = "block";
        document.getElementById("bottom").style.display = "block";
    } else {
        document.getElementById("filling_select1").style.display = "block";
        document.getElementById("filling_select1_2").style.display = "none";
        document.getElementById("filling_select2").style.display = "none";
        document.getElementById("height2_div").style.display = "none";
        document.getElementById("without").style.display = "block";
        document.getElementById("top").style.display = "none";
        document.getElementById("bottom").style.display = "none";
    }
}

function set_twosidded() {
    var twosidded = document.door_form.elements["one_or_two"].checked;
    if (twosidded) {
        document.getElementById("color1").style.display = "none";
        document.getElementById("color1_2").style.display = "block";
        document.getElementById("color2").style.display = "block";
    } else {
        document.getElementById("color1").style.display = "block";
        document.getElementById("color1_2").style.display = "none";
        document.getElementById("color2").style.display = "none";
    }
}
/*
отображаем полученные рузультаты
*/
function displayResults(full_price) {
    var result = document.getElementById("cost_price");
    result.innerHTML = full_price;
}

function select_color(color) {
    var color_file = colors[color];
    var leaf = document.getElementById("leaf");
    var door = document.getElementById("door");
    var hinges = document.getElementById("hinges");
    leaf.style.backgroundImage = "url(\"i/colors/" + color_file + "\")";
    door.style.backgroundImage = "url(\"i/box/" + color_file + "\")";
    hinges.style.backgroundImage = "url(\"i/hinges/" + color_file + "\")";
    set_knob_color(color);
}

function set_knob_color(color) {
    var color_file = colors[color];
    var selected_knob = document.door_form.elements["handle"].value;
    var knob_div = document.getElementById("knob");
    var path;
    if (selected_knob != 1) {
        knob_div.style.backgroundImage = "url(\"i/knobs/" + selected_knob + "/" + color_file + "\")";
    }
}

function set_filling(filling, place) {
    var filling_file = fillings[filling];
    switch (place) {
        case "full":
            var without = document.getElementById("without");
            without.style.backgroundImage = "url(\"i/fillings/without/" + filling_file + "\")";
            break;
        case "top":
            var top = document.getElementById("top");
            top.style.backgroundImage = "url(\"i/fillings/top/" + filling_file + "\")";
            break;
        case "bottom":
            var bottom = document.getElementById("bottom");
            bottom.style.backgroundImage = "url(\"i/fillings/bottom/" + filling_file + "\")";
            break;
    }
}

function set_closer(closer) {
    var closer_div = document.getElementById("closer");
    if (closer) {
        closer_div.style.display = "block";
    } else {
        closer_div.style.display = "none";
    }
}

function set_knob(knob) {
    //get color
    var color = document.door_form.elements["coloring1"].value;
    var color_file = colors[color];
    var knob_div = document.getElementById("knob");
    knob_file = "url(\"i/knobs/1/knob.png\")";
    if (knob != 1) {
        knob_file = "url(\"i/knobs/" + knob + "/" + color_file + "\")";
    }
    knob_div.style.backgroundImage = knob_file;
    /*
    var knob_file = knobs[knob];
    var knob_div = document.getElementById("knob");
    knob_div.style.backgroundImage = "url(\"i/" + knob_file + "\")";
    */
}