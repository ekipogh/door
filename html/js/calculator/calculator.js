var A = 189.6; //Стоврка
var B = 158.9; //Рама
var C = 150.55; //Импост
var D = 37.66; //Штапик
var I = 70.76; //порог
var raspil_coefficient = 1.15 //f
var G = 360; //петля
var H = 250; //замок
var J = 218.59; //цилиндр
var K = 50; //навстречник
var L = 150; //нажимной гарнитур
var M = 750; //доводчик
var O = 455 //ручка скобка
var P = 2000 //сборка
var R = 6.97; //уплотнитель 9GO/42p
var Q = 5.63; //уплотнитель 9GO/04
var T = 51.4; //угловая закладная
var S = 25.53; //импостная закладная
var filling_prices = [600, 900, 400, 1000, 1200, 1100, 1800, 1200, 2200, 1600]; //цены на заполнение в соответсвие с выподающим списком на странице калькулятора
var coloring = [1.14, 1.4, 1.5, 1.5, 1.5, 1.5, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.6, 1.4];
var furniture_prices = [150, 750, 455];
/*расчитываем стоимость и выводим результат
impos - bool c импостом или без
height - float выслота всей двери, или если с импостом, то верхней части
height2 - float высота нижней части
length - float длина
amount - int количество дверей
handle - int 0 нажимной гарнитур, 1 - доводчик и ручка
filling1 - int индекс заполнения верхней части(с имостом), либо всей двери
filling2 - int индекс заполнения нижней части
*/
function calculate(impost, height, height2, length, amount, coloring1, handle, closer, filling1, filling2, one_side) {
    var profile = amount * get_profile(height, height2, length, impost); //цена профиля
    var coloring_coefficient = get_coloring_coefficient(one_side, coloring1); //коэффициент цвета
    var accessories = amount * get_accessories(impost); //цена коплектующих
    var seals = amount * get_seals(height, height2, length, impost); //цена уплотнителей
    var fittings = amount * get_fittings(handle, closer); //цена фурнитуры
    var complite_profile = round(profile * raspil_coefficient * coloring_coefficient); //профиль с покарской
    var clear = round(complite_profile + seals + fittings + accessories); //дверь без установки и заполнения
    var installment = amount * P; //цена установки
    var filling = amount * get_filling(height, height2, length, filling1, filling2, impost); //цена заполнения
    var with_installation_fillings = round(clear + installment + filling); //цена с установкой и заполнением
    return round(with_installation_fillings + with_installation_fillings * 0.345);
    //displayResults(profile, complite_profile, accessories, seals, fittings, clear, with_installation_fillings); //отображаем результат
}
/*
функция расчета стоимости заполнения
если дверь с импостом:
верхнее заполнение (h - h1 - 0.140)*(l-0.2314)
нижнее заполнение (h1 - 0.140)*(l-0.2314)
если без:
заполнение полностью (l-0.2314)*(h-0.1907)
*/
function get_filling(height, height2, length, filling1, filling2, impost) {
    if (impost) {
        return (height - height2 - 0.140) * (length - 0.2314) * filling_prices[filling1] + (height2 - 0.140) * (length - 0.2314) * filling_prices[filling2];
    } else {
        return (length - 0.2314) * (height - 0.1907) * filling_prices[filling1];
    }
}
/*
расчитываем стоимость фурнитуры
*/
function get_fittings(handle, closer) {
    var handle_val = furniture_prices[handle] + (closer ? M : 0);
    return round(2 * G + 1 * H + 1 * J + 1 * K + handle_val);
}
/*
расчитываем стоимость уплотнителей
*/
function get_seals(height, height2, length, impost) {
    var stvorka = get_stvorka(height, length);
    var rama = get_rama(height, length);
    var shtapik = get_shtapik(height, height2, length, impost);
    return round((stvorka + rama + shtapik) * R + shtapik * Q);
}
/*
расчитываем стоимость комплектующих
*/
function get_accessories(impost) {
    if (impost) {
        return 6 * T + 2 * S;
    } else {
        return 6 * T;
    }
}
/*
возвращает коэффициент покраски в зависимости от выбраннаго цвета
*/
function get_coloring_coefficient(one_side, coloring1) {
    if (!one_side) {
        return coloring[coloring1];
    } else {
        return 1.7;
    }
}
/*
расчитывает метраж профиля
*/
function get_profile(height, height2, length, impost) {
    var stvorka = round(get_stvorka(height, length) * A);
    var rama = round(get_rama(height, length) * B);
    var shtapik = round(get_shtapik(height, height2, length, impost) * D);
    var impost_val = round(impost_valF(length) * C);
    var porog = round(get_porog(length) * I);
    return stvorka + rama + shtapik + porog + ((impost) ? impost_val : 0);
}
/*
округяет цены до копейки
*/
function round(money) {
    return Math.round(money * 100) / 100;
}

function get_porog(length) {
    return (length - 0.108);
}

function get_shtapik(height, height2, length, impost) {
    if (impost) {
        return (4 * (length - 0.225) + 2 * (height - height2 - 0.162) + 2 * (height2 - 0.121));
    } else {
        return (2 * (length - 0.213) + 2 * (height - 0.225));
    }
}

function get_rama(height, length) {
    return (2 * height + 2 * length);
}

function get_stvorka(height, length) {
    return ((2 * height - 0.113) + (2 * length - 0.194));
}

function impost_valF(length) {
    return (length - 0.225);
}